<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas de Selección</h1>
    </div>

    <div class="body-content">

        <div class="row">
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 1 </h4>
                        <p> Listar las edades de los ciclistas sin repetidos </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>                    
                </div>                
            </div>

            <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 2 </h4>
                        <p> Listar las edades de los ciclistas de Artiach </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>                    
                </div>                
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 3 </h4>
                        <p> Listar las edades de los ciclistas de Artiach o de Amore Vita </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>                    
                </div>                
            </div>          
            
        </div>
    </div>
</div>
